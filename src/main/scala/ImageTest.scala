import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import Launcher.noises_generator

/**
  * To test the noise generator
  */
class ImageTest {

  /**
    * The image's width
    */
  val image_width = 50

  /**
    * The image's height
    */
  val image_height = 50

  /**
    * The buffer image, used to draw the noise into the image
    */
  val buffer_image = new BufferedImage(image_width, image_height, BufferedImage.TYPE_INT_RGB)


  /**
    * Conceptually maps the noise map to the image.
    * Draws the noise into the image
    */
  def drawNoiseIntoTheImage() : Unit = {

    val (noise_map, max_noise_val) = noises_generator.frequencyNoise(image_height, image_width)  // The noise map (mapped to the image) and the maximal value of the noise, within the all noise map

    (0 until image_height).foreach(y =>
      (0 until image_width).foreach(x => {
        buffer_image.setRGB(
          x,
          y,
          noises_generator.getColor(x, y, noise_map, max_noise_val).getRGB
        )
      }))

    ImageIO.write(buffer_image, "png", new File("image_of_a_noise.png"))
  }

}
