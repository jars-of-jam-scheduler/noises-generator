object Launcher {

  /**
    * Noises generator
    */
  val noises_generator : NoisesGenerator = new NoisesGenerator

  /**
    * The image writer
    */
  val image_test : ImageTest = new ImageTest

  def main(args : Array[String]) : Unit = {

    image_test.drawNoiseIntoTheImage()
    new ControlWindow().display()

  }
}
