import java.awt.Graphics
import javax.swing._
import Launcher.{noises_generator, image_test}

/**
  * Window allowing the user to interfere with the noise generator
  */
class ControlWindow extends JFrame {

  /**
    * Displays the window
    */
  def display(): Unit = {
    setTitle("Control of the noises maker")
    setSize(400, 400)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    setVisible(true)
  }

  /**
    * Fulfill the window to make the user able to interferer with the noise generator
    * @param graphics the Graphics object
    */
  override def paint(graphics : Graphics): Unit = {
    super.paint(graphics)

    val label_1 : JLabel = new JLabel("Coef. de fréquence :")
    val label_2 : JLabel = new JLabel("Multiplicateur du coef. de fréquence :")
    val label_3 : JLabel = new JLabel("Coef. d'amplitude :")
    val label_4 : JLabel = new JLabel("Multiplicateur du coef. d'amplitude :")


    val spinner_1 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.05))
    val spinner_2 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.05))
    val spinner_3 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.05))
    val spinner_4 = new JSpinner(new SpinnerNumberModel(0, -1, 1, 0.05))

    val layout = new BoxLayout(this.getContentPane, BoxLayout.Y_AXIS)
    this.getContentPane.setLayout(layout)

    this.add(label_1)
    this.add(spinner_1)
    this.add(label_2)
    this.add(spinner_2)
    this.add(label_3)
    this.add(spinner_3)
    this.add(label_4)
    this.add(spinner_4)


    spinner_1.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]
      if(new_val != 0) {
        noises_generator.FREQ_COEF += new_val
        image_test.drawNoiseIntoTheImage()
      }
      spinner_1.setValue(0.0)
    })

    spinner_2.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]
      if(new_val != 0) {
        noises_generator.FREQ_COEF_MULT += new_val
        image_test.drawNoiseIntoTheImage()
      }
      spinner_2.setValue(0.0)
    })

    spinner_3.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]
      if(new_val != 0) {
        noises_generator.AMPL_COEF += new_val
        image_test.drawNoiseIntoTheImage()
      }
      spinner_3.setValue(0.0)
    })

    spinner_4.addChangeListener(e => {
      val new_val : Double = e.getSource.asInstanceOf[JSpinner].getValue.asInstanceOf[Double]
      if(new_val != 0) {
        noises_generator.AMPL_COEF_MULT += new_val
        image_test.drawNoiseIntoTheImage()
      }
      spinner_4.setValue(0.0)
    })

  }

}
