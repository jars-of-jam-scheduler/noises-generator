import java.awt.Color

/**
  * Generates 2D noises
  */
class NoisesGenerator {

  /**
    * The number of frequency layers
    */
  var NUMBER_OF_LAYERS = 5

  /**
    * Coefficient for the frequency
    */
  var FREQ_COEF : Double = 0.02

  /**
    * Multiplies the coefficient for the frequency
    */
  var FREQ_COEF_MULT : Double = 2

  /**
    * Coefficient for the amplitude
    */
  var AMPL_COEF : Double = 1.0

  /**
    * Multiplies the coefficient for the amplitude
    */
  var AMPL_COEF_MULT : Double = 0.5

  /**
    * The length of the random numbers array
    */
  val length_random_numbers : Int = 256

  /**
    * Contains the random numbers to interpolate between
    */
  val random_numbers : Seq[Seq[Double]] = Seq.fill(length_random_numbers)(Seq.fill(length_random_numbers)(scala.math.random()))

  /**
    * Used to interpolate between random numbers
    */
  val linear_interpolator = new LinearInterpolator

  /**
    * Interpolates between the four values corresponding to the vertices of a square, defined according to the provided
    * point
    *
    * @param point a point that belongs to the image, provided with positive coordinates
    * @return the point interpolated between the four values corresponding to the vertices of a square, defined
    *         according to the provided point
    */
  def startInterpolation(point : Seq[Double]) : Double = {

    val x_min_bound : Int = point.head.toInt
    val y_min_bound : Int = point(1).toInt

    val random_values = Seq(  // Contains 4 points, each provided with only 1 coordinate (which is the random value)
      Seq(random_numbers(x_min_bound % length_random_numbers)(y_min_bound % length_random_numbers)),
      Seq(random_numbers((x_min_bound + 1) % length_random_numbers)(y_min_bound % length_random_numbers)),
      Seq(random_numbers(x_min_bound % length_random_numbers)((y_min_bound + 1) % length_random_numbers)),
      Seq(random_numbers((x_min_bound + 1) % length_random_numbers)((y_min_bound + 1) % length_random_numbers))
    )

    linear_interpolator.computeTheLinearlyInterpolatedPointBetween4Points(random_values, point.head - x_min_bound, point(1) - y_min_bound, linear_interpolator.cosine).head
  }

  /**
    * Computes the frequency noise for a given image.
    *
    * @param image_height the image's height
    * @param image_width the image's width
    */
  def frequencyNoise(image_height : Int, image_width : Int): (Seq[Seq[Double]], Double) = {
    val noise_map: collection.mutable.Seq[collection.mutable.Seq[Double]] = collection.mutable.Seq.fill(image_height + 1)(collection.mutable.Seq.fill(image_width + 1)(0.0))

    var max_noise_val = 0.0
    (0 to image_height).foreach(y => {
      (0 to image_width).foreach(x => {

        var (point: collection.mutable.Seq[Double], amplitude: Double) = (collection.mutable.Seq(x * FREQ_COEF, y * FREQ_COEF), AMPL_COEF) // Multiplying by an arbitrary frequency
        (0 until NUMBER_OF_LAYERS).foreach(_ => {  // For each noise's layer
          noise_map(y)(x) += startInterpolation(point) * amplitude
          point(0) *= FREQ_COEF_MULT
          point(1) *= FREQ_COEF_MULT
          amplitude *= AMPL_COEF_MULT
        })

        if(noise_map(y)(x) > max_noise_val) {  // Normalizing
          max_noise_val = noise_map(y)(x)
        }
      })})

    (noise_map, max_noise_val)
  }

  def getColor(x: Int, y : Int, noise_map : Seq[Seq[Double]], max_noise_val : Double) : Color = {
    new Color(
      (noise_map(y)(x)/max_noise_val).floatValue(),
      (noise_map(y)(x)/max_noise_val).floatValue(),
      (noise_map(y)(x)/max_noise_val).floatValue()
    )
  }

  def setNumberOfLayers(new_number_of_layers : Int): Unit = {
    NUMBER_OF_LAYERS = new_number_of_layers
  }

}
